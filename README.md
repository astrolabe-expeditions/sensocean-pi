# Sensocean for Raspberry-PI

## Install

```
sudo apt install python-virtualenv
cd ~
mkdir .venv
virtualenv --python=/usr/bin/python3 .venv/sensocean
source .venv/sensocean/bin/activate
pip install pip -U
pip install git+https://framagit.org/astrolabe-expeditions/sensocean-pi
```

## Usage

```
sensocean loop --help
Sensocean version 0.0.0
usage: sensocean loop [-h] [--delay DELAY] [--ec_address EC_ADDRESS]
                      [--rtd_address RTD_ADDRESS] [--gps_port GPS_PORT]

optional arguments:
  -h, --help            show this help message and exit
  --delay DELAY         Delay between two measure, in seconds (default: 1.5)
  --ec_address EC_ADDRESS
                        Address of the salinity probe (default: 100)
  --rtd_address RTD_ADDRESS
                        Address of the temperature probe (default: 102)
  --gps_port GPS_PORT   UART port for reading GPS NMEA stream (default:
                        /dev/ttyAMA0)
```

Example:

    sensocean loop --delay 5
