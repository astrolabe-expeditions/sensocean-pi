import fcntl
import io
import time

from .utils import log


class I2C:
    LONG_TIMEOUT = 1.5          # timeout for readings and calibrations
    SHORT_TIMEOUT = .5          # timeout for regular commands
    DEFAULT_BUS = 1             # default bus for I2C (older boards use bus 0)
    DEFAULT_ADDRESS = 98        # default sensor address
    CURRENT_ADDR = DEFAULT_ADDRESS

    def __init__(self, address=DEFAULT_ADDRESS, bus=DEFAULT_BUS):
        path = "/dev/i2c-" + str(bus)
        try:
            self.file_read = io.open(path, "rb", buffering=0)
            self.file_write = io.open(path, "wb", buffering=0)
        except:
            log.critical('Unable to open IRC streams')
            raise

        self.set_i2c_address(address)

    def set_i2c_address(self, addr):
        # Set the I2C communications to the slave specified by the address.
        # The commands for I2C dev using the ioctl functions are specified in
        # the i2c-dev.h file from i2c-tools.
        I2C_SLAVE = 0x703
        fcntl.ioctl(self.file_read, I2C_SLAVE, addr)
        fcntl.ioctl(self.file_write, I2C_SLAVE, addr)
        self.CURRENT_ADDR = addr

    def write(self, cmd):
        cmd += "\00"  # Appends the null character.
        self.file_write.write(cmd.encode('ascii'))

    def read(self, num_of_bytes=31):
        res = self.file_read.read(num_of_bytes)  # Read from the board.
        # Remove the null characters.
        response = list(filter(lambda x: x != '\x00', res))
        if response[0] != 1:
            return "Error " + str(response[0])
        # Change MSB to 0 for all received characters except the first
        # and get a list of characters.
        char_list = map(lambda x: chr(x & ~0x80), list(response[1:]))
        # Having to change the MSB to 0 is a glitch in the raspberry pi,
        # and you shouldn't have to do this!
        return ''.join(char_list)

    def query(self, string):
        self.write(string)
        # Read and calibration commands require a longer timeout.
        if string.upper().startswith(("R", "CAL")):
            time.sleep(self.LONG_TIMEOUT)
        elif string.upper().startswith("SLEEP"):
            return "sleep mode"
        else:
            time.sleep(self.SHORT_TIMEOUT)
        return self.read()

    def close(self):
        self.file_read.close()
        self.file_write.close()
