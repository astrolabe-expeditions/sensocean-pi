#!/usr/bin/env python

import argparse
from pathlib import Path

from sensocean import VERSION
from sensocean.main import loop, I2C


def main():
    if VERSION:
        print('Sensocean version', VERSION)
    main_parser = argparse.ArgumentParser(description='Sensocean CLI.')
    subparsers = main_parser.add_subparsers(title='Available commands',
                                            metavar='')
    parser = subparsers.add_parser(
        'loop',
        help='Loop and poll data.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--delay', type=int, default=I2C.LONG_TIMEOUT,
                        help='Delay between two measure, in seconds')
    parser.add_argument('--ec-address', type=int, default=100,
                        help="Address of the salinity probe")
    parser.add_argument('--rtd-address', type=int, default=102,
                        help='Address of the temperature probe')
    parser.add_argument('--gps-port', default='/dev/ttyAMA0',
                        help='UART port for reading GPS NMEA stream')
    parser.add_argument('--data-filepath',
                        default=Path('/home/pi/sensocean.csv'),
                        help='Path to CSV file for writing data', type=Path)
    parser.set_defaults(func=loop)

    args = main_parser.parse_args()
    if getattr(args, 'func', None):
        args.func(args)
    else:
        main_parser.print_help()


if __name__ == '__main__':
    main()
