import csv
import time

import serial

from .gps import read_gps
from .i2c import I2C
from .utils import log, abort


def loop(args):
    """Loop, read sensors and write data to a CSV."""
    log.info('Starting the loop')
    EC_SENSOR = I2C(args.ec_address)
    RTD_SENSOR = I2C(args.rtd_address)
    GPS = serial.Serial(args.gps_port, baudrate=9600, timeout=3)

    delay = args.delay
    if delay < I2C.LONG_TIMEOUT:
        log.info("Polling time is shorter than timeout, "
                 "setting polling time to %0.2f" % I2C.LONG_TIMEOUT)
        delay = I2C.LONG_TIMEOUT

    def do_loop():
        gps = read_gps(GPS)

        try:
            ec = EC_SENSOR.query("R")
        except OSError:
            abort('Unable to connect to EC probe.')
        try:
            rtd = RTD_SENSOR.query("R")
        except OSError:
            abort('Unable to connect to RTD probe.')
        data = [gps.datetime.isoformat(), ec, rtd, str(gps.latitude),
                str(gps.longitude)]
        print(';'.join(data))

        with args.data_filepath.open(mode='a') as f:
            writer = csv.writer(f, delimiter=';')
            writer.writerow(data)

        # Delay between 2 acquistions
        time.sleep(delay)

    while True:
        try:
            do_loop()
        except KeyboardInterrupt:
            EC_SENSOR.close()
            RTD_SENSOR.close()
            log.info('Stopping the loop')
            break
