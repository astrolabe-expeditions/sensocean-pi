import logging
import logging.handlers
import sys


log = logging.getLogger('sensocean')
log.setLevel(logging.DEBUG)
handler = logging.handlers.SysLogHandler(address='/dev/log')
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
log.addHandler(handler)


def abort(msg):
    log.critical('{} - Aborting'.format(msg))
    sys.exit(1)
