from pynmea2 import parse
from pynmea2.nmea import ParseError

from .utils import log


def read_gps(conn):
    while True:
        buffer = conn.readline()
        try:
            msg = parse(buffer.decode())
        except (ParseError, UnicodeDecodeError) as e:
            # We are in the middle of a packet.
            log.warning('GPS NMEA parsing error')
            continue
        else:
            if msg.sentence_type == 'RMC':
                if not msg.is_valid:
                    log.warning('Invalid GPS message: %s', msg)
                    continue
                return msg
